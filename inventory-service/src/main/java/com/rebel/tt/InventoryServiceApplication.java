package com.rebel.tt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * InventoryServiceApplication --- Main class to start Inventory Service.
 */
@SpringBootApplication
public class InventoryServiceApplication {
    /**
     * Starts spring boot application.
     * @param args - A string array containing the command line arguments.
     */
    public static void main(String[] args) {
        SpringApplication.run(InventoryServiceApplication.class, args);
    }
}
