package com.rebel.tt.labels;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class CaseCodeInfo {
    private String qrCodeId;
    private String batchNo;
    private LocalDate mfgDate;
    private LocalDate expDate;
    private String netWeightValue;
    private String netWeightInfo;
    private String category;
    private String type;
    private String skuName;
    private String group;
    private String rebelSkuCode;
    private String grossWeight;
    private String venderSkuCode;
}
