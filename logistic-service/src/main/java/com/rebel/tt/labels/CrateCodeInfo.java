package com.rebel.tt.labels;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CrateCodeInfo {
    private String qrCodeId;
    private String qrCodeValue;
}
