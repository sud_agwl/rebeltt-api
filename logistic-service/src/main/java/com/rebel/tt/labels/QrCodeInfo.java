package com.rebel.tt.labels;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QrCodeInfo {
    private String qrCodeId;
    private String qrCodeValue;
    private Long sparkId;
    private LocalDate mfgDate;
    private LocalDate expDate;
    private double weight;
    private String category;
    private String rebelSkuCode;
    private String vendorSkuCode;
    private String skuName;
    private String batchNo;
    private String group;
    private String userId;
    private String userType;

    private String type;
    private String netWeightValue;
    private String netWeightInfo;
    private String grossWeight;
    private String mfgName;
    private LocalDate labelPrintedAt;
    private Long groupQuantity;
}
