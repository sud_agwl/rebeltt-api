package com.rebel.tt.validation.impl;

import com.rebel.tt.dto.PrintSkuLabelRequestDto;
import com.rebel.tt.validation.MfgExpDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MfgExpDateValidator implements ConstraintValidator<MfgExpDate, PrintSkuLabelRequestDto> {
    @Override
    public boolean isValid(PrintSkuLabelRequestDto value, ConstraintValidatorContext constraintValidatorContext) {
        if (value.getMfgDate() != null && value.getExpDate() != null) {
            return value.getMfgDate().isBefore(value.getExpDate());
        } else return true;
    }
}
