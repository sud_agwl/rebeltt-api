package com.rebel.tt.validation;

import com.rebel.tt.validation.impl.MfgExpDateValidator;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * MfgExpDate --- Interface to implement date check custom validator
 */
@Constraint(validatedBy = MfgExpDateValidator.class)
@Target({ TYPE })
@Retention(RUNTIME)
public @interface MfgExpDate {
    String message() default "Expiry date must be before manufacturing date";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
