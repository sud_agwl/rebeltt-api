package com.rebel.tt.controller;

import com.rebel.tt.dto.PrintSkuLabelRequestDto;
import com.rebel.tt.service.LabelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Base64;

/**
 * LabelController --- To generate labels for packages, crates and cases.
 */
@RestController
@Slf4j
public class LabelController {

    /**
     * The label service
     */
    @Autowired
    private LabelService labelService;

    /**
     * Print sku label
     *
     * @param skuLabelRequestDto - Request body for printing labels.
     * @param userId - Identifier for user.
     * @param userType - Supplier, dc or kitchen.
     * @return - Encoded string to generate QR code.
     */
    @PostMapping(value = "/print_sku_labels")
    @ResponseBody
    public String printSkuLabels(@Valid @RequestBody PrintSkuLabelRequestDto skuLabelRequestDto,
                                 @RequestHeader(name = "Authorization") String userId, @RequestHeader(name = "userType") String userType,
                                 BindingResult result) {
        log.info("REST request to print SKU labels: {}", skuLabelRequestDto);
        skuLabelRequestDto.setUserId(userId);
        skuLabelRequestDto.setUserType(userType);

        return Base64.getEncoder().encodeToString(labelService.printSkuLabels(skuLabelRequestDto));
    }
}
