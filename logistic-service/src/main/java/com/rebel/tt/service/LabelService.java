package com.rebel.tt.service;

import com.rebel.tt.dto.PrintSkuLabelRequestDto;

/**
 * LabelService --- The label service interface.
 */
public interface LabelService {
    /**
     * Print sku label
     *
     * @param skuLabelRequestDto - Request body for printing labels.
     * @return - Encoded string to generate QR code
     */
    byte[] printSkuLabels(PrintSkuLabelRequestDto skuLabelRequestDto);
}
