package com.rebel.tt.service.impl;

import com.rebel.tt.dto.PrintSkuLabelRequestDto;
import com.rebel.tt.labels.QrCodeInfo;
import com.rebel.tt.service.LabelService;
import com.rebel.tt.utils.PojoToPojoMapper;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * LabelServiceImpl --- The label service implementation.
 */
@Service
@Slf4j
public class LabelServiceImpl implements LabelService {
    /**
     * To generate the QR code from http request, insert data into packages table and finally sending QR code back to client.
     *
     * @param skuLabelRequestDto - Request body for printing labels.
     * @return - Byte array containing QR code details.
     */
    @Override
    @Transactional
    public byte[] printSkuLabels(PrintSkuLabelRequestDto skuLabelRequestDto) {
        List<QrCodeInfo> qrCodeInfoList = new ArrayList<>();

        for (int i = 0; i < skuLabelRequestDto.getQuantity(); i++) {
            QrCodeInfo qrCodeInfo = (QrCodeInfo) PojoToPojoMapper.qrCodeInfoObjectMapper(skuLabelRequestDto);
            updateLabelDetails(qrCodeInfo);
            qrCodeInfoList.add(qrCodeInfo);
        }

        byte[] pdfFile = printPdf(qrCodeInfoList,"SkuReport.jasper");

        return pdfFile;
    }

    private void updateLabelDetails(QrCodeInfo qrCodeInfo) {
        int seq = 1;

        qrCodeInfo.setQrCodeId(randomNumber(qrCodeInfo.getCategory(),"01"));

        String labelInfo = String.valueOf(seq++) + "=" + qrCodeInfo.getQrCodeId() + ",";
        labelInfo += String.valueOf(seq++) + "=" + qrCodeInfo.getCategory() + ",";
        labelInfo += String.valueOf(seq++) + "=" + qrCodeInfo.getRebelSkuCode() + ",";
        labelInfo += String.valueOf(seq++) + "=" + qrCodeInfo.getVendorSkuCode() + ",";
        labelInfo += String.valueOf(seq++) + "=" + qrCodeInfo.getBatchNo() + ",";
        labelInfo += String.valueOf(seq++) + "=" + qrCodeInfo.getMfgDate() + ",";
        labelInfo += String.valueOf(seq++) + "=" + qrCodeInfo.getExpDate() + ",";
        labelInfo += String.valueOf(seq++) + "=" + "01" + ",";
        labelInfo += String.valueOf(seq++) + "=" + qrCodeInfo.getGroup() + ",";
        labelInfo += String.valueOf(seq++) + "=" + qrCodeInfo.getSkuName() + ",";
        labelInfo += String.valueOf(seq) + "=" + qrCodeInfo.getWeight();

        qrCodeInfo.setQrCodeValue(labelInfo);
    }

    private String randomNumber(String category,String type) {
        long randomNo = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;

        return new StringBuilder("RF-" + type + "-" + category + "-" + randomNo).toString();
    }

    private byte[] printPdf(List<QrCodeInfo> labelList, String fileName){
        byte[] labelFile = new byte[0];
        try {
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(labelList);
            HashMap<String,Object> parameters = new HashMap<>();
            parameters.put("CollectionBeanParameter",dataSource);
            InputStream is = new ClassPathResource(fileName).getInputStream();
            JasperReport report = (JasperReport) JRLoader.loadObject(is);
            JasperPrint jasperPrint = JasperFillManager.fillReport(report,parameters,new JREmptyDataSource());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint,byteArrayOutputStream);
            labelFile = byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred while trying to build QrCode", e);
        }

        return labelFile;
    }
}
