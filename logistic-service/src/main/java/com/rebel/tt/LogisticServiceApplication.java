package com.rebel.tt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * LogisticServiceApplication --- Main class to start Logistic Service.
 */
@SpringBootApplication
public class LogisticServiceApplication {
    /**
     * Starts spring boot application.
     * @param args - A string array containing the command line arguments.
     */
    public static void main(String[] args) {
        SpringApplication.run(LogisticServiceApplication.class, args);
    }
}
