package com.rebel.tt.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.rebel.tt.validation.MfgExpDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * PrintLabelRequestDto --- http request dto for printing label.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@MfgExpDate
public class PrintLabelRequestDto implements Serializable {

    private Long id;
    private Long sparkId;
    private LocalDate mfgDate;
    private LocalDate expDate;
    private double weight;
    private String category;
    private String rebelSkuCode;
    private String vendorSkuCode;
    private String type;
    private String skuName;
    private String batchNo;

    @Range(min = 1, max = 1000, message = "Quantity should be inside range 1 to 1000")
    private Long quantity;
    private Long groupQuantity;
    private String netWeightValue;
    private String netWeightInfo;
    private String grossWeight;
    private String mfgName;
    private String group;
    private LocalDate labelPrintedAt;
    private String userId;
    private String userType;
}
