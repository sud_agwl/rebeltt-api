package com.rebel.tt.exception;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.OffsetDateTime;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApiErrorModel {
    private OffsetDateTime timeStamp;
    private HttpStatus httpStatus;
    private List<String> message;
}
