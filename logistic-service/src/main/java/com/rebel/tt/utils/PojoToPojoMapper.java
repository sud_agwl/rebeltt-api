package com.rebel.tt.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rebel.tt.labels.QrCodeInfo;

public class PojoToPojoMapper {

    public static Object qrCodeInfoObjectMapper(Object obj) {
        ObjectMapper objMapper = new ObjectMapper();
        objMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objMapper.findAndRegisterModules();
        QrCodeInfo qrCodeInfo = objMapper.convertValue(obj, QrCodeInfo.class);
        return qrCodeInfo;
    }
}
