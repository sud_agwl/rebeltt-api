package com.rebel.tt.enums;

public enum PackageStatus {

    PENDING,
    IN_CASE,
    IN_CRATE,
    IN_TRANSIT_TO_DC,
    AT_DC,
    REPACKAGED_AT_DC,
    DISPATCH_READY_TO_KITCHEN,
    IN_TRANSIT_TO_KITCHEN,
    AT_KITCHEN,
    USED_AT_KITCHEN
}
