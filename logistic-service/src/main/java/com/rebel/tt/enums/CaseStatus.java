package com.rebel.tt.enums;

public enum CaseStatus {

    PENDING,
    DISPATCH_READY_TO_DC,
    IN_TRANSIT_TO_DC,
    AT_DC,
    OPENED_AT_DC,
    EMPTY,
    DISPATCH_READY_TO_KITCHEN,
    IN_TRANSIT_TO_KITCHEN,
    AT_KITCHEN,
    USED_AT_KITCHEN
}
