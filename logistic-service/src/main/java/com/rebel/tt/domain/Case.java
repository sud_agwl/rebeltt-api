package com.rebel.tt.domain;

import com.rebel.tt.enums.CaseStatus;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "cases")
@Data
public class Case extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long sparkSkuId;
    private String batchId;
    private String weight;
    private String comments;

    @Enumerated(EnumType.STRING)
    private CaseStatus status;
    @Column(name = "label_id", unique = true)
    private String labelId;
    private String labelInfo;
    private String rebelSkuCode;

    private Long labelPrintedAt;
    private Long manufacturerPackagedAt;
    private Long manufacturerDispatchedAt;
    private Long supplierInwardAt;
    private Long supplierRepackagedAt;
    private Long supplierDispatchedAt;
    private Long kitchenInwardAt;

    private String sparkManufacturerId;
    private String sparkSupplierId;
    private String sparkKitchenId;
    private Long sparkDcId;

    private String dcPoNumber;
    private String dcInvoiceNumber;
    private String dcCode;
    private String dcLocation;
    private String kitchenPoNumber;
    private String kitchenInvoiceNumber;
    private String kitchenCode;
    private String kitchenLocation;
}
