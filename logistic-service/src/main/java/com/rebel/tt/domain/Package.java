package com.rebel.tt.domain;

import com.rebel.tt.enums.PackageStatus;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "packages")
@Data
public class Package extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long sparkSkuId;
    private String batchId;
    private Long manufacturedAt;
    private Long expiresAt;
    private String weight;
    private String comments;

    @Enumerated(EnumType.STRING)
    private PackageStatus status;
    @Column(name = "label_id", unique = true)
    private String labelId;
    private String labelInfo;
    private String rebelSkuCode;

    private Long labelPrintedAt;
    private Long manufacturerCasedAt;
    private Long manufacturedDispatchedAt;
    private Long supplierInwardAt;
    private Long supplierRepackagedAt;
    private Long supplierDispatchedAt;
    private Long kitchenInwardAt;
    private Long kitchenUsedAt;
    private Long usedAt;

    private String sparkManufacturerId;
    private String sparkSupplierId;
    private String sparkKitchenId;

    @ManyToOne
    @JoinColumn(name = "supplier_case_id")
    private Case supplierCase;

    @ManyToOne
    @JoinColumn(name = "manufacturer_case_id")
    private Case manufacturerCase;

    @ManyToOne
    @JoinColumn(name = "crate_id")
    private Crate crate;

    private String dcPoNumber;
    private String dcInvoiceNumber;
    private String dcCode;
    private String dcLocation;
    private String kitchenPoNumber;
    private String kitchenInvoiceNumber;
    private String kitchenCode;
    private String kitchenLocation;
}
