package com.rebel.tt.domain;

import com.rebel.tt.enums.CrateStatus;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "crates")
@Data
public class Crate extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long sparkSkuId;
    @Enumerated(EnumType.STRING)
    private CrateStatus status;
    private String comments;

    @Column(name = "label_id")
    private String labelId;
    private String labelInfo;

    @ManyToOne
    @JoinColumn(name = "case_id")
    private Case case_;

    private Long labelPrintedAt;
    private Long usedAt;
    private Long packagedAt;
    private String sparkManufacturerId;
}
